#
# GitLab CI: Android v0.2
#
# Based on
# https://hub.docker.com/r/jangrewe/gitlab-ci-android/
# https://git.faked.org/jan/gitlab-ci-android
#

FROM ubuntu:20.04
MAINTAINER Chris Hallgren <chris@hallgren.net>

ENV VERSION_SDK_TOOLS "9123335"
ENV VERSION_BUILD_TOOLS "33.0.1"
ENV VERSION_TARGET_SDK "33"
ENV VERSION_CMAKE "3.22.1"

ENV ANDROID_HOME "/sdk"
ENV SKD_ROOT="/sdk"
ENV PATH "$PATH:${ANDROID_HOME}/tools:${ANDROID_HOME}/build-tools/${VERSION_BUILD_TOOLS}:${ANDROID_HOME}/cmdline-tools/bin"
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -qq update && \
    apt-get install -qqy --no-install-recommends \
      curl \
      openssl \
      openssh-client \
      git \
      html2text \
      openjdk-11-jdk \
      libc6-i386 \
      lib32stdc++6 \
      lib32gcc1 \
      lib32z1 \
      unzip \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN /var/lib/dpkg/info/ca-certificates-java.postinst configure
ADD https://dl.google.com/android/repository/commandlinetools-linux-${VERSION_SDK_TOOLS}_latest.zip /tools.zip
RUN unzip /tools.zip -d /sdk && \
    rm -v /tools.zip

RUN yes | sdkmanager --sdk_root=/sdk "build-tools;${VERSION_BUILD_TOOLS}" "platforms;android-${VERSION_TARGET_SDK}" "cmake;${VERSION_CMAKE}" "platform-tools"
